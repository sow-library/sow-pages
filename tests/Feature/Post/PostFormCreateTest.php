<?php
namespace Tests\Feature;

use Auth;

use PostSeeder;

use App\Models\Post;
use App\Models\User;

beforeEach(function () {
    Post::truncate();

    $this->seed(PostSeeder::class);

    $this->route = "/admin/posts/create";
    $this->component = "Admin/posts/create";

    $this->user = User::factory()->create();
});

// [!!! Auth !!!]
it('it_should_forbid_an_unauthenticated_user_from_seeing_post_create_form', function () {
    $this->get($this->route)
        ->assertStatus(302)
        ->assertRedirectContains('login')
    ;
});

// [Auth]
it('it_should_forbid_an_authenticated_non_Admin_from_seeing_post_create_form', function () {
    $this->actingAs(Auth::loginUsingId(2))->get($this->route)
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
});

// [Auth::Admin] && Component:   'Admin/posts/create'
it('it_should_allow_an_authenticated_Admin_to_see_the_post_create_form', function () {
    $theComponent = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->assertSuccessful()
        ->viewData('page')['component']
    ;
    expect($theComponent)->toEqual($this->component);
});
