<?php

namespace Tests\Feature;

use Auth;

use App\Models\Post;
use App\Models\User;

use PostSeeder;
use UserSeeder;

beforeEach(function () {
    Post::truncate();
    User::truncate();

    $this->seed(PostSeeder::class);
    $this->seed(UserSeeder::class);

    $this->route = "/admin/posts";
    $this->component = "Admin/posts/index";

    $this->thePostSchema = [
         'parent',
        'published',
        'title',
        'slug',
        'author',
        'categories',
        'tags',
        'content',
        'image',
        'status',
    ];
});

// [!!! Auth !!!]
it('can_NOT_see_the_posts_index_when_logged_out', function () {
    $this->get($this->route)
        ->assertStatus(302)
        ->assertRedirectContains('login');
});

// [Auth]
it('can_see_the_posts_index_when_logged_in', function () {
    $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->assertStatus(200)
        ->assertSuccessful();
});

// Component:   'Admin/posts/index'
it('posts_index_url_returns_Admin_posts_index_Component_when_logged_in', function () {
    $theComponent = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['component'];

    expect($theComponent)->toEqual($this->component);
});

it('returns_correct_Post_schema', function () {
    $thePostData = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['props']['Data']['Posts']['data'];

    expect($thePostData[0])->toHaveKeys($this->thePostSchema);
    expect($thePostData[1])->toHaveKeys($this->thePostSchema);
    expect($thePostData[2])->toHaveKeys($this->thePostSchema);
});

it('returns_empty_Post_Trash_schema', function () {
    $theTrashData = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['props']['Data']['Trash']['data'];

    expect($theTrashData)->toEqual([]);
});

it('returns_correct_Post_Trash_schema', function () {
    $this->actingAs(Auth::loginUsingId(1))->get($this->route . "/trash/3");
    $this->actingAs(Auth::loginUsingId(1))->get($this->route . "/trash/4");
    $this->actingAs(Auth::loginUsingId(1))->get($this->route . "/trash/5");


    $theTrashData = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['props']['Data']['Trash']['data'];

    expect($theTrashData[0])->toHaveKeys($this->thePostSchema);
    expect($theTrashData[1])->toHaveKeys($this->thePostSchema);
    expect($theTrashData[2])->toHaveKeys($this->thePostSchema);
});
