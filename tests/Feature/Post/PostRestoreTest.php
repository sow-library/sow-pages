<?php
namespace Tests\Feature;

use PostSeeder;
use UserSeeder;

use App\Models\Post;
use App\Models\User;

use Auth;

beforeEach(function () {
    Post::truncate();
    User::truncate();

    $this->seed(UserSeeder::class);
    $this->seed(PostSeeder::class);

    $this->routeRestore = "/admin/posts/restore/";
    $this->routeTrash = "/admin/posts/trash/";
});


it('it_should_forbid_an_unauthenticated_user_from_restoring_a_post_entry', function () {
    $response = $this->json('GET', $this->routeRestore."1")
        ->assertUnauthorized();
});

it('it_should_redirect_an_authenticated_user_from_restoring_someone_elses_post_entry', function () {

    // TODO: ROLE_AUTH

    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."3");
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."4");
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."5");

    $this->actingAs(Auth::loginUsingId(2))->get($this->routeRestore."3")
        ->assertStatus(302)
        ->assertRedirectContains('login')
    ;
    $this->actingAs(Auth::loginUsingId(2))->get($this->routeRestore."4")
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
    $this->actingAs(Auth::loginUsingId(2))->get($this->routeRestore."5")
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
});


// TODO WHY IS IT NOT DOING A REAL TEST OF AUTH::ADMIN!?!?!
