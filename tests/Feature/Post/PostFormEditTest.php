<?php
namespace Tests\Feature;

use PostSeeder;
use UserSeeder;

use App\Models\Post;
use App\Models\User;

use Auth;

beforeEach(function () {
    Post::truncate();
    User::truncate();

    $this->seed(PostSeeder::class);
    $this->seed(UserSeeder::class);

    $this->route = "/admin/posts/edit/3";
    $this->component = "Admin/posts/edit";

    $this->thePostSchema = [
        'parent',
        'published',
        'title',
        'slug',
        'author',
        'categories',
        'tags',
        'content',
        'image',
        'status',
    ];
});

// [!!! Auth !!!]
it('posts_edit_url_is_redirected_when_logged_out', function () {
    $response = $this->get($this->route)
        ->assertStatus(302)
        ->assertRedirectContains('login')
    ;
});

// [Auth]
it('posts_edit_url_form_redirects_non_Admin_when_logged_in', function () {
    $theComponent = $this->actingAs(Auth::loginUsingId(2))->get($this->route)
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
});

// [Auth::Admin]
it('posts_edit_url_form_shown_when_Admin_is_logged_in', function () {
    $theComponent = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->assertStatus(200)
        ->assertSuccessful()
    ;
});


// Component:   'Admin/posts/edit'
it('posts_edit_url_returns_Admin_posts_edit_Component_when_logged_in', function () {
    $theComponent = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['component']
    ;

    expect($theComponent)->toEqual($this->component);
});


// [Data.Post]
it('sends_correct_Post_schema_to_Edit_Form', function (){
    $thePostArray = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['props']['Data']['Post']
    ;

    expect($thePostArray)->toHaveKeys($this->thePostSchema);
});

