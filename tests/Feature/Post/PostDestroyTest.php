<?php
namespace Tests\Feature;

// use RefreshDatabase;

use PostSeeder;
use UserSeeder;

use App\Models\Post;
use App\Models\User;

use Auth;

beforeEach(function () {
    Post::truncate();
    User::truncate();

    $this->seed(UserSeeder::class);
    $this->seed(PostSeeder::class);

    $this->routeDestroy = "/admin/posts/";
    $this->routeTrash = "/admin/posts/trash/";
});

it('it_should_forbid_an_unauthenticated_user_from_destroying_a_Post_entry', function () {
    $this->get($this->routeDestroy."1")
        ->assertStatus(302)
        ->assertRedirectContains('login')
    ;
});

it('it_should_redirect_an_authenticated_non_Admin_from_destroying_a_Post', function () {
    Post::truncate();
    $this->seed(PostSeeder::class);

    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."1");
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."2");
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."3");

    $this->actingAs(Auth::loginUsingId(2))->delete($this->routeDestroy."1")
        ->assertStatus(302)
        ->assertRedirectContains('login')
    ;
    $this->actingAs(Auth::loginUsingId(2))->delete($this->routeDestroy."2")
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
    $this->actingAs(Auth::loginUsingId(2))->delete($this->routeDestroy."3")
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
});

it('it_should_allow_an_authenticated_Admin_to_destroy_a_Post_entry', function () {
    Post::truncate();
    $this->seed(PostSeeder::class);
    
    // TODO: ROLE_AUTH
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."1");
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."2");
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."3");

    $this->actingAs(Auth::loginUsingId(1))->delete($this->routeDestroy."1")
        ->assertStatus(302)
        ->assertRedirectContains('post')
    ;
    $this->actingAs(Auth::loginUsingId(1))->delete($this->routeDestroy."2")
        ->assertStatus(302)
        ->assertRedirectContains('post')
    ;
    $this->actingAs(Auth::loginUsingId(1))->delete($this->routeDestroy."3")
        ->assertStatus(302)
        ->assertRedirectContains('post')
    ;
});
