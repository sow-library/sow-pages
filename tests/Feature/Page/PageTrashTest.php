<?php
namespace Tests\Feature;

use PageSeeder;

use App\Models\Page;
use App\Models\User;

use Auth;

beforeEach(function () {
    Page::truncate();
    $this->seed(PageSeeder::class);

    $this->route = "/admin/pages/trash/";
});

it('it_should_redirect_an_unauthenticated_user_as_unauthorized_from_trashing_a_page', function () {
    $response = $this->json('GET', $this->route."3")
        ->assertUnauthorized();
});


it('it_should_redirect_an_authenticated_non_Admin_user_as_unauthorized_from_trashing_a_page', function () {
    $this->actingAs(Auth::loginUsingId(2))->get($this->route."3")
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
    $this->actingAs(Auth::loginUsingId(2))->get($this->route."4")
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
    $this->actingAs(Auth::loginUsingId(2))->get($this->route."5")
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
});

it('it_should_allow_an_authenticated_Admin_to_trash_a_page_entry', function () {
    $this->actingAs(Auth::loginUsingId(1))->get($this->route."3")
        ->assertStatus(302)
        ->assertRedirectContains('pages')
    ;
    $this->actingAs(Auth::loginUsingId(1))->get($this->route."4")
        ->assertStatus(302)
        ->assertRedirectContains('pages')
    ;
    $this->actingAs(Auth::loginUsingId(1))->get($this->route."5")
        ->assertStatus(302)
        ->assertRedirectContains('pages')
    ;
});
