<?php

namespace Tests\Feature;

use Auth;

use App\Models\Page;
use App\Models\User;

use PageSeeder;
use UserSeeder;

beforeEach(function () {
    Page::truncate();
    User::truncate();

    $this->seed(PageSeeder::class);
    $this->seed(UserSeeder::class);

    $this->route = "/admin/pages";
    $this->component = "Admin/pages/index";

    $this->thePageSchema = [
        'title',
        'parent',
        'cs_id',
        'slug',
        'author',
        'categories',
        'tags',
        'content',
        'released',
        'image',
        'status',
    ];
});

// [!!! Auth !!!]
it('can_NOT_see_the_pages_index_when_logged_out', function () {
    $this->get($this->route)
        ->assertStatus(302)
        ->assertRedirectContains('login');
});

// [Auth]
it('can_see_the_pages_index_when_logged_in', function () {
    $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->assertStatus(200)
        ->assertSuccessful();
});

// Component:   'Admin/pages/index'
it('pages_index_url_returns_Admin_pages_index_Component_when_logged_in', function () {
    $theComponent = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['component'];

    expect($theComponent)->toEqual($this->component);
});

it('returns_correct_Page_schema', function () {
    $thePageData = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['props']['Data']['Pages']['data'];

    expect($thePageData[0])->toHaveKeys($this->thePageSchema);
    expect($thePageData[1])->toHaveKeys($this->thePageSchema);
    expect($thePageData[2])->toHaveKeys($this->thePageSchema);
});

it('returns_empty_Page_Trash_schema', function () {
    $theTrashData = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['props']['Data']['Trash']['data'];

    expect($theTrashData)->toEqual([]);
});

it('returns_correct_Page_Trash_schema', function () {
    $this->actingAs(Auth::loginUsingId(1))->get($this->route . "/trash/3");
    $this->actingAs(Auth::loginUsingId(1))->get($this->route . "/trash/4");
    $this->actingAs(Auth::loginUsingId(1))->get($this->route . "/trash/5");


    $theTrashData = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['props']['Data']['Trash']['data'];

    expect($theTrashData[0])->toHaveKeys($this->thePageSchema);
    expect($theTrashData[1])->toHaveKeys($this->thePageSchema);
    expect($theTrashData[2])->toHaveKeys($this->thePageSchema);
});
