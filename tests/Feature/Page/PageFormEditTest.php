<?php
namespace Tests\Feature;

use PageSeeder;
use UserSeeder;

use App\Models\Page;
use App\Models\User;

use Auth;

beforeEach(function () {
    Page::truncate();
    User::truncate();

    $this->seed(PageSeeder::class);
    $this->seed(UserSeeder::class);

    $this->route = "/admin/pages/edit/3";
    $this->component = "Admin/pages/edit";

    $this->thePageSchema = [
        'title',
        'parent',
        'cs_id',
        'slug',
        'author',
        'categories',
        'tags',
        'content',
        'released',
        'image',
        'status',
    ];
});

// [!!! Auth !!!]
it('pages_edit_url_is_redirected_when_logged_out', function () {
    $response = $this->get($this->route)
        ->assertStatus(302)
        ->assertRedirectContains('login')
    ;
});

// [Auth]
it('pages_edit_url_form_redirects_non_Admin_when_logged_in', function () {
    $theComponent = $this->actingAs(Auth::loginUsingId(2))->get($this->route)
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
});

// [Auth::Admin]
it('pages_edit_url_form_shown_when_Admin_is_logged_in', function () {
    $theComponent = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->assertStatus(200)
        ->assertSuccessful()
    ;
});


// Component:   'Admin/pages/edit'
it('pages_edit_url_returns_Admin_pages_edit_Component_when_logged_in', function () {
    $theComponent = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['component']
    ;

    expect($theComponent)->toEqual($this->component);
});


// [Data.Page]
it('sends_correct_Page_schema_to_Edit_Form', function (){
    $thePageArray = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->viewData('page')['props']['Data']['Page']
    ;

    expect($thePageArray)->toHaveKeys($this->thePageSchema);
});

