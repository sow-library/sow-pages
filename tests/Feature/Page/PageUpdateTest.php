<?php
namespace Tests\Feature;

use UserSeeder;
use PageSeeder;

use App\Models\User;
use App\Models\Page;

use Auth;

beforeEach(function () {
    User::truncate();
    Page::truncate();

    $this->seed(UserSeeder::class);
    $this->seed(PageSeeder::class);

    $this->route = "/admin/pages/";
    $this->status = [0, 1];

    $this->thePageSchema = [
        'title',
        'parent',
        'cs_id',
        'slug',
        'author',
        'categories',
        'tags',
        'content',
        'released',
        'image',
        'status',
    ];
});

it('it_should_redirect_unauthenticated_users_from_updating_a_page', function () {
    $arrFaker = [
        'parent'        => "0",
        'title'         => "The Page Title",
        'slug'          => "the-page-slug",

        'author'        => "1",
        'categories'    => "Baking",
        'tags'          => "meat, cheese, potatoes",
        'content'       => "I am the content",

        'status'        => array_rand($this->status, 1)
    ];

    $response = $this->json('PUT', $this->route."3", $arrFaker)
        ->assertUnauthorized()
    ;
});

it('it_should_redirect_an_authenticated_non_Admin_from_updating_a_page', function () {
    $arrFaker = [
        'parent'        => "0",
        'title'         => "The Page Title",
        'slug'          => "the-page-slug",

        'author'        => "1",
        'categories'    => "Baking",
        'tags'          => "meat, cheese, potatoes",
        'content'       => "I am the content",

        'status'        => array_rand($this->status, 1)
    ];

    $this->actingAs(Auth::loginUsingId(2))->put($this->route."3", $arrFaker)
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
});

it('returns_correct_initial_Pages_Data_schema', function () {
    $beforeChange = [
        'parent'        => "0",
        'title'         => "Privacy Statement",
        'slug'          => "privacy-statement",

        'author'        => "1",
        'categories'    => "categories",
        'tags'          => "tags",
//        'content'       => "<p><img class=\" size-full wp-image-14\" alt=\"Privacy Statement Image - hands hiding content of content\" data-entity-type=\"file\" data-entity-uuid=\"72983845-942d-4520-be4a-67211e5275dc\" src=\"http:\/\/wp.hockey\/wp-content\/uploads\/2016\/08\/privacy.jpg\" width=\"200\" height=\"132\" \/><\/p>\n\n<p>Thank you for visiting our web site. This privacy policy tells you how we use personal information collected at this site. Please read this privacy policy before using the site or submitting any personal information. By using the site, you are accepting the practices described in this privacy policy. These practices may be changed, but any changes will be posted and changes will only apply to activities and information on a going forward, not retroactive basis. You are encouraged to review the privacy policy whenever you visit the site to make sure that you understand how any personal information you provide will be used.<\/p>\n\n<p>Note: the privacy practices set forth in this privacy policy are for this web site only. If you link to other web sites, please review the privacy policies posted at those sites.<\/p>\n\n<p><strong>Collection of Information<\/strong><\/p>\n\n<p>We collect personally identifiable information, like names, postal addresses, email addresses, etc., when voluntarily submitted by our visitors. The information you provide is used to fulfill you specific request. This information is only used to fulfill your specific request, unless you give us permission to use it in another manner, for example to add you to one of our mailing lists.<\/p>\n\n<p><strong>Cookie\/Tracking Technology<\/strong><\/p>\n\n<p>The Site may use cookie and tracking technology depending on the features offered. Cookie and tracking technology are useful for gathering information such as browser type and operating system, tracking the number of visitors to the Site, and understanding how visitors use the Site. Cookies can also help customize the Site for visitors. Personal information cannot be collected via cookies and other tracking technology, however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.<\/p>\n\n<p>Distribution of Information<\/p>\n\n<p>We may share information with governmental agencies or other companies assisting us in fraud prevention or investigation. We may do so when: (1) permitted or required by law; or, (2) trying to protect against or prevent actual or potential fraud or unauthorized transactions; or, (3) investigating fraud which has already taken place. The information is not provided to these companies for marketing purposes.<\/p>\n\n<p><strong>Commitment to Data Security<\/strong><\/p>\n\n<p>Your personally identifiable information is kept secure. Only authorized employees, agents and contractors (who have agreed to keep information secure and confidential) have access to this information. All emails and newsletters from this site allow you to opt out of further mailings.<\/p>\n\n<p><strong>Privacy Contact Information<\/strong><\/p>\n\n<p>If you have any questions, concerns, or comments about our privacy policy you may contact us using the Contact Us page.<\/p>\n",
    ];

    $pagesEntry = Page::find(1);

    expect($pagesEntry)->toMatchArray($beforeChange);
});


it('it_validates_pages_update_data_and_does_not_save_changes', function () {
    $arrFakerNull = [];

    $nullResponse = $this->actingAs(Auth::loginUsingId(1))->put($this->route."3", $arrFakerNull)
        ->assertStatus(302)
    ;

    $nullResponse
        ->assertSessionHasErrors(['parent'])
        ->assertSessionHasErrors(['title'])
        ->assertSessionHasErrors(['slug'])

        ->assertSessionHasErrors(['author'])
        ->assertSessionHasErrors(['categories'])
        ->assertSessionHasErrors(['tags'])
        ->assertSessionHasErrors(['content'])
    ;
});


it('it_allows_an_authenticated_Admin_to_update_a_page', function () {
    $faker = \Faker\Factory::create();

    $beforeChange = [
        'parent'        => "0",
        'title'         => "Privacy Statement",
        'slug'          => "privacy-statement",

        'author'        => "1",
        'categories'    => "categories",
        'tags'          => "tags",
        'content'       => "<p><img class=\" size-full wp-image-14\" alt=\"Privacy Statement Image - hands hiding content of content\" data-entity-type=\"file\" data-entity-uuid=\"72983845-942d-4520-be4a-67211e5275dc\" src=\"http:\/\/wp.hockey\/wp-content\/uploads\/2016\/08\/privacy.jpg\" width=\"200\" height=\"132\" \/><\/p>\n\n<p>Thank you for visiting our web site. This privacy policy tells you how we use personal information collected at this site. Please read this privacy policy before using the site or submitting any personal information. By using the site, you are accepting the practices described in this privacy policy. These practices may be changed, but any changes will be posted and changes will only apply to activities and information on a going forward, not retroactive basis. You are encouraged to review the privacy policy whenever you visit the site to make sure that you understand how any personal information you provide will be used.<\/p>\n\n<p>Note: the privacy practices set forth in this privacy policy are for this web site only. If you link to other web sites, please review the privacy policies posted at those sites.<\/p>\n\n<p><strong>Collection of Information<\/strong><\/p>\n\n<p>We collect personally identifiable information, like names, postal addresses, email addresses, etc., when voluntarily submitted by our visitors. The information you provide is used to fulfill you specific request. This information is only used to fulfill your specific request, unless you give us permission to use it in another manner, for example to add you to one of our mailing lists.<\/p>\n\n<p><strong>Cookie\/Tracking Technology<\/strong><\/p>\n\n<p>The Site may use cookie and tracking technology depending on the features offered. Cookie and tracking technology are useful for gathering information such as browser type and operating system, tracking the number of visitors to the Site, and understanding how visitors use the Site. Cookies can also help customize the Site for visitors. Personal information cannot be collected via cookies and other tracking technology, however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.<\/p>\n\n<p>Distribution of Information<\/p>\n\n<p>We may share information with governmental agencies or other companies assisting us in fraud prevention or investigation. We may do so when: (1) permitted or required by law; or, (2) trying to protect against or prevent actual or potential fraud or unauthorized transactions; or, (3) investigating fraud which has already taken place. The information is not provided to these companies for marketing purposes.<\/p>\n\n<p><strong>Commitment to Data Security<\/strong><\/p>\n\n<p>Your personally identifiable information is kept secure. Only authorized employees, agents and contractors (who have agreed to keep information secure and confidential) have access to this information. All emails and newsletters from this site allow you to opt out of further mailings.<\/p>\n\n<p><strong>Privacy Contact Information<\/strong><\/p>\n\n<p>If you have any questions, concerns, or comments about our privacy policy you may contact us using the Contact Us page.<\/p>\n",
    ];

    $arrFaker = [
        'parent'        => "0",
        'title'         => "The Page Title",
        'slug'          => "the-page-slug",

        'author'        => "1",
        'categories'    => "Baking",
        'tags'          => "meat, cheese, potatoes",
        'content'       => "I am the content",

        'status'        => array_rand($this->status, 1)
    ];

    $this->actingAs(Auth::loginUsingId(1))->put($this->route."3", $arrFaker)
        ->assertStatus(302)
        ->assertRedirectContains('pages')
    ;

    $pagesEntry = Page::find(1);

    expect($pagesEntry)->not->toMatchArray($beforeChange);
});
