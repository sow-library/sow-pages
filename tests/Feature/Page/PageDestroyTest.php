<?php
namespace Tests\Feature;

// use RefreshDatabase;

use PageSeeder;
use UserSeeder;

use App\Models\Page;
use App\Models\User;

use Auth;

beforeEach(function () {
    Page::truncate();
    User::truncate();

    $this->seed(UserSeeder::class);
    $this->seed(PageSeeder::class);

    $this->routeDestroy = "/admin/pages/";
    $this->routeTrash = "/admin/pages/trash/";
});

it('it_should_forbid_an_unauthenticated_user_from_destroying_a_Page_entry', function () {
    $this->get($this->routeDestroy."3")
        ->assertStatus(302)
        ->assertRedirectContains('login')
    ;
});

it('it_should_redirect_an_authenticated_non_Admin_from_destroying_a_Page', function () {
    Page::truncate();
    $this->seed(PageSeeder::class);

    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."3");
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."4");
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."5");

    $this->actingAs(Auth::loginUsingId(2))->delete($this->routeDestroy."3")
        ->assertStatus(302)
        ->assertRedirectContains('login')
    ;
    $this->actingAs(Auth::loginUsingId(2))->delete($this->routeDestroy."4")
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
    $this->actingAs(Auth::loginUsingId(2))->delete($this->routeDestroy."5")
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
});

it('it_should_allow_an_authenticated_Admin_to_destroy_a_Page_entry', function () {
    Page::truncate();
    $this->seed(PageSeeder::class);
    
    // TODO: ROLE_AUTH
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."3");
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."4");
    $this->actingAs(Auth::loginUsingId(1))->get($this->routeTrash."5");

    $this->actingAs(Auth::loginUsingId(1))->delete($this->routeDestroy."3")
        ->assertStatus(302)
        ->assertRedirectContains('page')
    ;
    $this->actingAs(Auth::loginUsingId(1))->delete($this->routeDestroy."4")
        ->assertStatus(302)
        ->assertRedirectContains('page')
    ;
    $this->actingAs(Auth::loginUsingId(1))->delete($this->routeDestroy."5")
        ->assertStatus(302)
        ->assertRedirectContains('page')
    ;
});
