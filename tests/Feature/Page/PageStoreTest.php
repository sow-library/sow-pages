<?php
namespace Tests\Feature;

use Auth;

use App\Models\Page;

beforeEach(function () {
    $this->route = "/admin/pages/";
    $this->status = [0, 1];

    $this->arrFaker = [
        'parent'        => "0",
        'title'         => "The Page Title",
        'slug'          => "the-page-slug",

        'author'        => "1",
        'categories'    => "Baking",
        'tags'          => "meat, cheese, potatoes",
        'content'       => "I am the content",

        'status'        => array_rand($this->status, 1)
    ];
});

// [!!! Auth !!!]
it('it_should_forbid_an_unauthenticated_user_from_creating_a_pages_entry', function () {
    $response = $this->json('POST', $this->route, $this->arrFaker)
        ->assertUnauthorized()
    ;
});

// [Auth]
it('it_should_forbid_an_authenticated_non_Admin_from_creating_a_pages_entry', function () {
    $this->actingAs(Auth::loginUsingId(2))->json('POST', $this->route, $this->arrFaker)
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
});

// [Auth::Admin]
it('it_validates_submitted_data_and_creates_new_page_by_Admin_only', function () {
    Page::truncate();

    $this->actingAs(Auth::loginUsingId(1))->post($this->route, $this->arrFaker)
        ->assertStatus(302)
        ->assertRedirectContains('pages')
    ;
});

it('it_invalidates_submitted_data_and_returns_correct_error_messages', function () {
    $arrFakerNull = [];

    $nullResponse = $this->actingAs(Auth::loginUsingId(1))->post($this->route, $arrFakerNull)
        ->assertStatus(302)
    ;

    $nullResponse
        ->assertSessionHasErrors(['parent'])
        ->assertSessionHasErrors(['title'])
        ->assertSessionHasErrors(['slug'])

        ->assertSessionHasErrors(['author'])
        ->assertSessionHasErrors(['categories'])
        ->assertSessionHasErrors(['tags'])
        ->assertSessionHasErrors(['content'])
    ;
});
