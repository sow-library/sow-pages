<?php
namespace Tests\Feature;

use Auth;

use PageSeeder;

use App\Models\Page;
use App\Models\User;

beforeEach(function () {
    Page::truncate();

    $this->seed(PageSeeder::class);

    $this->route = "/admin/pages/create";
    $this->component = "Admin/pages/create";

    $this->user = User::factory()->create();
});

// [!!! Auth !!!]
it('it_should_forbid_an_unauthenticated_user_from_seeing_page_create_form', function () {
    $this->get($this->route)
        ->assertStatus(302)
        ->assertRedirectContains('login')
    ;
});

// [Auth]
it('it_should_forbid_an_authenticated_non_Admin_from_seeing_page_create_form', function () {
    $this->actingAs(Auth::loginUsingId(2))->get($this->route)
        ->assertStatus(302)
        ->assertRedirectContains('unauthorized')
    ;
});

// [Auth::Admin] && Component:   'Admin/pages/create'
it('it_should_allow_an_authenticated_Admin_to_see_the_page_create_form', function () {
    $theComponent = $this->actingAs(Auth::loginUsingId(1))->get($this->route)
        ->assertSuccessful()
        ->viewData('page')['component']
    ;
    expect($theComponent)->toEqual($this->component);
});
