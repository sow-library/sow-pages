<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

use Sipofwater\SowPages\Http\Controllers\PageController;
use Sipofwater\SowPages\Models\Page;


//Route::get('/pages',        [PageController::class, 'sayHello'])->name('pages.sayHello');
//Route::get('/pages/{post}', [PageController::class, 'show'])->name('pages.show');
//Route::post('/pages',       [PageController::class, 'store'])->name('pages.store');

Route::get('/', function () {
    $pagingPerPage = 10;

    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,

        'Pages' => Page::orderBy('title', 'desc')
            ->where('parent', '=', '80')
            ->where('status', '=', '1')
            ->latest()
            ->paginate($pagingPerPage),

       /*  'Posts' => Post::orderBy('title', 'asc')
            ->where('status', '=', '1')
            ->latest()
            ->paginate($pagingPerPage), */
    ]);
});

// ============== STATIC PAGES ============
Route::inertia('/pages/about',          'Public/About');
Route::inertia('/pages/advertise',      'Public/Advertise');
Route::inertia('/pages/contact',        'Public/Contact');
Route::inertia('/pages/privacy',        'Public/Privacy');
Route::inertia('/pages/returns',        'Public/Returns');
Route::inertia('/pages/shipping',       'Public/Shipping');


// ============== PRODUCT PAGES ============
//Route::get('/pages/products',           [ProductPagesController::class, 'product_listing'])->name('productpages.index');
//Route::get('/pages/products/{slug}',    [ProductPagesController::class, 'product_details'])->name('productpages.details');


// ============== BLOG ============
//Route::get('/blog',                     [ProductPagesController::class, 'blog_listing'])->name('blog.index');
//Route::get('/blog/{id}',                [ProductPagesController::class, 'blog_post'])->name('blog.post');





//////////////////////////////////////////

/* Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
}); */

Route::get('/admin/unauthorized', function () {
    return Inertia::render('Admin/Unauthorized');
})->name('unauthorized');

Route::middleware(['auth', 'verified'])->group(function () {

    Route::controller(PageController::class)
        ->prefix('admin/pages')
        ->as('pages.')
        ->group(function () {
            Route::get('', 'index')->name('index');
            Route::get('/create', 'create')->name('create');
            Route::get('/edit/{page}', 'edit')->name('edit');

            Route::post('', 'store')->name('store');
            Route::get('/{page}', 'show')->name('show');
            Route::put('/{page}', 'update')->name('update');
            Route::delete('/{page}', 'destroy')->name('destroy');
            Route::get('/restore/{page}', 'restore')->name('restore');
            Route::get('/trash/{page}', 'trash')->name('trash');
        })
    ;

    /* Route::controller(PostController::class)
        ->prefix('admin/posts')
        ->as('posts.')
        ->group(function () {
            Route::get('', 'index')->name('index');
            Route::get('/create', 'create')->name('create');
            Route::get('/edit/{page}', 'edit')->name('edit');

            Route::post('', 'store')->name('store');
            Route::get('/{page}', 'show')->name('show');
            Route::put('/{page}', 'update')->name('update');
            Route::delete('/{page}', 'destroy')->name('destroy');
            Route::get('/restore/{page}', 'restore')->name('restore');
            Route::get('/trash/{page}', 'trash')->name('trash');
        })
    ; */

});
