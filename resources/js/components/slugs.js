
export function generateSlug(data) {
    return data
        .trim()
        .replace(/[^\w\s]/gi, '')
        .toLowerCase()
        .replaceAll(' ', '-')
    ;
}
