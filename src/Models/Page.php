<?php
//namespace App\Models;
namespace Sipofwater\SowPages\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model {
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'title',
                'except',       // DOES NOT EXIST!
        'parent',       // page_id
        'author',       // user_id
        'slug',
        'content',

        'image',
        'status',
        
        // taxonomy - shouldn't be stored here!
        'categories',   // category_id
        'tags',         // tag_id

        // Pages Only:
        'cs_id',
        'released',
/*
        // Post Only:
        'published',
*/
    ];

    public function parentInfo() {
        return $this->belongsTo(Cardset::class, 'parent', 'pid');
    }

    public function author() {
        return $this->belongsTo(User::class, 'author');
    }

    public function checklist() {
        return $this->belongsTo(Cardset::class, 'cs_id');
    }

    public function tagcloud() {
        return $this->hasMany(Taxonomy::class, 'cmspage_id');
    }
}