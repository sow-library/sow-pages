<?php
namespace Sipofwater\SowPages\Providers;

use Illuminate\Support\ServiceProvider;

class PagePackageServiceProvider extends ServiceProvider {
    public function boot() {
        
        if ($this->app->runningInConsole()) {
            // Export the migration
            if (! class_exists('CreatePagesTable')) {
                $this->publishes([
                    __DIR__ . '/../../database/migrations/create_pages_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_pages_table.php'),
                    // you can add any number of migrations here
                ], 'migrations');
            }
        }

        $this->loadMigrationsFrom(__DIR__.'/../../dasdfasdfasdfatabase/migrations');
        
//        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');


    }
}
