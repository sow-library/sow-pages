<?php
// namespace App\Http\Controllers;
namespace Sipofwater\SowPages\Http\Controllers;


use App\Http\Controllers\Controller;
use Request;
use Redirect;
use Inertia\Inertia;

use App\Models\Page;

class PageController extends Controller {
    public $pagingPerPage = 10;

    public function formPrep(Request $request) {
        return [
            'title' => $request->title,
                'except' => $request->except,
            'parent' => $request->parent,
            'author' => $request->author,
            'slug' => $request->slug,
            'content' => $request->content,
            
            'image' => $request->image,
            'status' => $request->status,

            // taxonomy - shouldn't be stored here!
            'categories' => $request->categories,       // category_id
            'tags' => $request->tags,                   // tag_id

            // Pages Only:
            'cs_id' => $request->cs_id,
            'released' => $request->released,

            'created_at' => $request->created_at,
            'updated_at' => now()
        ];
    }
    public $validators = [
        'parent' => 'required',
        'title' => 'required|unique:pages|max:100',
        'slug' => 'required|max:100',

        'author' => 'required',
        'categories' => 'required',
        'tags' => 'required',
        'content' => 'required',
    ];
    public $updateValidators = [
        'parent' => 'required',
        'title' => 'required|max:100',
        'slug' => 'required|max:100',

        'author' => 'required',
        'categories' => 'required',
        'tags' => 'required',
        'content' => 'required',
    ];
    public $errorsValidators = [
        'parent.required' => 'Please select the Parent',

        'title.required' => 'Page Title is required',
        'title.unique' => 'Please enter a unique title',

        'slug.required' => 'Slug is required',
        'author.required' => 'Please select an Author',
        'categories.required' => 'Please enter at least one category',
        'tags.required' => 'Please select some tags',
        'content.required' => 'Please enter some Content',
    ];

    public function sayHello() {
        return "Hello, World!";
    }

    // Admin Pages:
    public function index() {

        // TODO: ROLE_AUTH
        if (auth()->id() != 1) {
            return Redirect::route('unauthorized')->with('error', 'Unauthorized Access!');
        }

        $Data["Pages"] = Page::with(['checklist'])
            ->orderBy('title', 'asc')
            ->latest()
            ->paginate($this->pagingPerPage)
        ;
        $Data["Trash"] = Page::onlyTrashed()->latest()->paginate($this->pagingPerPage);
        return Inertia::render('Admin/pages/index', compact("Data"));
    }

    public function create() {

        // TODO: ROLE_AUTH
        if (auth()->id() != 1) {
            return Redirect::route('unauthorized')->with('error', 'Unauthorized Access!');
        }

        return Inertia::render('Admin/pages/create');
    }

    public function edit($id) {

        // TODO: ROLE_AUTH
        if (auth()->id() != 1) {
            return Redirect::route('unauthorized')->with('error', 'Unauthorized Access!');
        }

        $Data["Page"] = Page::with(['author', 'checklist', 'tagcloud', 'parentInfo'])
            ->find($id)
        ;
        return Inertia::render('Admin/pages/edit', compact("Data"));
    }

    public function store(Request $request) {

        // TODO: ROLE_AUTH
        if (auth()->id() != 1) {
            return Redirect::route('unauthorized')->with('error', 'Unauthorized Access!');
        }

        Page::create(
            Request::validate(
                $this->validators, 
                $this->errorsValidators
            )
        );
        return Redirect::route('pages.index')->with('success', 'Page Created Successfully.');
    }

    public function update(Request $request, $id) {

        // TODO: ROLE_AUTH
        if (auth()->id() != 1) {
            return Redirect::route('unauthorized')->with('error', 'Unauthorized Access!');
        }

        Page::where('id', $id)->update(
            Request::validate(
                $this->updateValidators, 
                $this->errorsValidators
            )
        );
        return Redirect::route('pages.index')->with('success', 'Page Updated Successfully.');
    }

    public function destroy($id) {

        // TODO: ROLE_AUTH
        if (auth()->id() != 1) {
            return Redirect::route('unauthorized')->with('error', 'Unauthorized Access!');
        }

        Page::onlyTrashed()->find($id)->forceDelete();
        return Redirect::route('pages.index')->with('success','Page Permanently Deleted');
    }

    // SoftDelete
    public function trash($id) {

        // TODO: ROLE_AUTH
        if (auth()->id() != 1) {
            return Redirect::route('unauthorized')->with('error', 'Unauthorized Access!');
        }

        Page::find($id)->delete();
        return Redirect::route('pages.index')->with('success', 'Page Soft Deleted Successfully');
    }

    public function restore($id) {

        // TODO: ROLE_AUTH
        if (auth()->id() != 1) {
            return Redirect::route('unauthorized')->with('error', 'Unauthorized Access!');
        }

        Page::withTrashed()->find($id)->restore();
        return Redirect::route('pages.index')->with('success', 'Page Restored Successfully');
    }

}
