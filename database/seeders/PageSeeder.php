<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Storage;

class PageSeeder extends Seeder {
    public function run() {
        $json = Storage::disk('local')->get('data/cmspages_pages.json');
        $json = json_decode($json);

        foreach ($json as $cmspage) {
            $status = ($cmspage->status == "publish") ? 1 : 0;

            DB::table('pages')->insert([
                //'id' => $cmspage->id,
                'title' => $cmspage->title,
                'parent' => $cmspage->parent,
                'cs_id' => $cmspage->cs_id,
                'slug' => $cmspage->slug,
                'author' => $cmspage->author,
                'content' => $cmspage->content,
                'released' => $cmspage->released,
                'status' => $status,

                'created_at' => now(),
                'updated_at' => now()
                ]);
        }
    }
}
